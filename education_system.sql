-- --------------------------------------------------------
-- 主機:                           203.64.84.213
-- 伺服器版本:                        8.0.19 - MySQL Community Server - GPL
-- 伺服器作業系統:                      Linux
-- HeidiSQL 版本:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 education_system 的資料庫結構
CREATE DATABASE IF NOT EXISTS `education_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `education_system`;

-- 傾印  資料表 education_system.archive 結構
CREATE TABLE IF NOT EXISTS `archive` (
  `courseId` int NOT NULL,
  `createdAt` bigint DEFAULT NULL,
  `fromModel` varchar(255) DEFAULT NULL,
  `originalRecord` longtext,
  `originalRecordId` longtext,
  PRIMARY KEY (`courseId`),
  UNIQUE KEY `courseId` (`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

-- 傾印  資料表 education_system.course 結構
CREATE TABLE IF NOT EXISTS `course` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `grade` enum('1','2','3','4','5','6','7','8','9') NOT NULL DEFAULT '1',
  `teacher` int NOT NULL,
  `department` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `courseId` (`id`),
  KEY `FK_course_department` (`department`),
  KEY `FK_course_teacher` (`teacher`),
  CONSTRAINT `FK_course_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_course_teacher` FOREIGN KEY (`teacher`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

-- 傾印  資料表 education_system.department 結構
CREATE TABLE IF NOT EXISTS `department` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departmentId` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

-- 傾印  資料表 education_system.elective 結構
CREATE TABLE IF NOT EXISTS `elective` (
  `id` int NOT NULL AUTO_INCREMENT,
  `score` int NOT NULL DEFAULT '0',
  `student` int NOT NULL,
  `course` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`student`,`course`),
  KEY `FK_elective` (`course`,`student`),
  CONSTRAINT `FK_elective_course` FOREIGN KEY (`course`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_elective_student` FOREIGN KEY (`student`) REFERENCES `student` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

-- 傾印  資料表 education_system.student 結構
CREATE TABLE IF NOT EXISTS `student` (
  `id` int NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `grade` enum('1','2','3','4','5','6','7','8','9') NOT NULL DEFAULT '1',
  `school_year` int NOT NULL,
  `department` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `studentId` (`id`),
  KEY `FK_student` (`department`),
  CONSTRAINT `FK_student_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

-- 傾印  資料表 education_system.teacher 結構
CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `department` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teacherId` (`id`),
  KEY `FK_teacher_department` (`department`),
  CONSTRAINT `FK_teacher_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- 取消選取資料匯出。

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
