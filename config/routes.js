/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },

  //student:選課、退課、查詢選課資訊、查詢成績
  //teacher:查詢課程資訊、查詢學生選課狀況、登記學生成績

  //課程管理
  'get /courses': 'course.show_courses',
  'get /course/:courseId': 'course.show_course',
  'get /courses/student/:studentId': 'course.show_student_courses',
  'get /courses/teacher/:teacherId': 'course.show_teacher_courses',
  'get /courses/department/:departmentId': 'course.show_department_courses',
  'post /course': 'course.add_course',
  'put /course/:courseId': 'course.update_course',
  'delete /course/:courseId': 'course.delete_course',
  //學生加退選課程
  'post /student/:studentId/course/:coursesId': 'elective.select_course',
  'delete /student/:studentId/course/:courseId': 'elective.unselect_course',
  
  //成績管理
  'put /score/student/:studentId/course/:courseId/_score/:score': 'elective.register_score',
  'get /scores/student/:studentId': 'elective.show_student_scores',
  'get /scores/course/:courseId': 'elective.show_course_scores',
  'get /scores': 'elective.show_scores',
  'get /score/student/:studentId/course/:courseId': 'elective.show_score',
  
  //學生管理
  'get /students': 'student.show_students',
  'get /student/:studentId': 'student.show_student',
  'get /students/course/:courseId': 'student.show_course_students',
  'get /students/department/:departmentId': 'student.show_department_students',
  'post /student': 'student.add_student',
  'put /student/:studentId': 'student.update_student',
  'delete /student/:studentId': 'student.delete_student',

  //教師管理
  'get /teachers': 'teacher.show_teachers',
  'get /teacher/:teacherId': 'teacher.show_teacher',
  'get /teacher/course/:courseId': 'teacher.show_course_teacher',
  'get /teachers/department/:departmentId': 'teacher.show_department_teachers',
  'post /teacher': 'teacher.add_teacher',
  'put /teacher/:teacherId': 'teacher.update_teacher',
  'delete /teacher/:teacherId': 'teacher.delete_teacher',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
