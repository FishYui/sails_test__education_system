# Education System

## Use
  - Language: Node.Js => Sails.js
  - DataBase: MySQL

## API
- ### Student API
  - [**Show Students**](#show-students)
  - [**Show Student**](#show-student)
  - [**Show Course's Students**](#show-course's-students)
  - [**Show Department's Students**](#show-department's-students)
  - [**Create Student**](#create-student)
  - [**Update Student**](#update-student)
  - [**Delete Student**](#Delete-student)
  
- ### Teacher API
  - [**Show Teachers**](#show-teachers)
  - [**Show Teacher**](#show-teacher)
  - [**Show Course's Teacher**](#show-course's-teacher)
  - [**Show Department's Teachers**](#show-department's-teachers)
  - [**Create Teacher**](#create-teacher)
  - [**Update Teacher**](#update-teacher)
  - [**Delete teacher**](#delete-teacher)
  
- ### Course API
  - [**Show Courses**](#show-courses)
  - [**Show Course**](#show-course)
  - [**Show Student's Courses**](#show-student's-courses)
  - [**Show Teacher's Courses**](#show-teacher's-courses)
  - [**Show Department's Courses**](#show-department's-courses)
  - [**Create Course**](#create-course)
  - [**Update Course**](#update-course)
  - [**Delete Course**](#delete-course)
  
- ### Elective API
  - [**Select Course**](#select-course)
  - [**UnSelect Course**](#unselect-course)
  - [**Register Score**](#register-score)
  - [**Show Student's Scores**](#show-student's-scores)
  - [**Show Course's Scores**](#show-course's-scores)
  - [**Show All Scores**](#show-all-scores)
  - [**Show One Score**](#show-one-score)

## MySQL Schema
  - [**Student**](#student)
  - [**Teacher**](#teacher)
  - [**Course**](#course)
  - [**Elective**](#elective)
  - [**Department**](#department)

## Note
  - [**Server Error**](#server-error)

---

# Api

  - ## Show Students

    Display information for all Students.

    - **URL** : `/students`

    - **Method** : `GET`

    - **Parameters** : `None`

    - **Success Response:**
      - **Code** : 200 
      - **Content Example**

        Display information for all Students.

        ```json
        {
            "success": true,
            "message": "Found All Students Success.",
            "response": [
                {
                    "id": 109311001,
                    "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                    "name": "王小明",
                    "grade": "1",
                    "school_year": 109,
                    "department": 311
                },
                {
                    "id": 109311002,
                    "password": "$2a$10$K5Z34avd2oyoGlSPL3jyO.TW0DcikhJQsbMt9jsK4xaN4EMUu/s7a",
                    "name": "陳小菲",
                    "grade": "1",
                    "school_year": 109,
                    "department": 311
                }, etc...
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Student

    Display information for a Student.

    - **URL** : `/student/:studentId`

    - **Method** : `GET`

    - **Parameters** : 

        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path"
            }
        ]
        ```

    - **Success Response:**

      - **Code** : 200 
      - **Content Example**
        
        Display information for Student with studentID 109311001.

        ```json
        {
            "success": true,
            "message": "Found Student Success.",
            "response": {
                "id": 109311001,
                "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                "name": "王小明",
                "grade": "1",
                "school_year": 109,
                "department": 311
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display information for Student with studentID 123321123.

        (student not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)
  
---

  - ## Show Course's Students

    Display all Student informations for a course.

    - **URL** : `/students/course/:courseId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Student informations for courseID 2311001.

        ```json
        {
            "success": true,
            "message": "Found All Course's Students Success.",
            "response": {
                "students": [
                    {
                        "id": 109311001,
                        "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                        "name": "王小明",
                        "grade": "1",
                        "school_year": 109,
                        "department": 311
                    }
                ],
                "id": 2311001,
                "name": "程式規劃",
                "grade": "1",
                "teacher": 1311001,
                "department": 311
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Student informations for courseID 123321123.

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Department's Students

    Display all Student informations for a department.

    - **URL** : `/students/department/:departmentId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "departmentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Department"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Student informations for a department with departmentID 311.

        ```json
        {
            "success": true,
            "message": "Found All Department's Students Success.",
            "response": [
                {
                    "students": [
                        {
                            "id": 109311001,
                            "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                            "name": "王小明",
                            "grade": "1",
                            "school_year": 109,
                            "department": 311
                        },
                        {
                            "id": 109311002,
                            "password": "$2a$10$K5Z34avd2oyoGlSPL3jyO.TW0DcikhJQsbMt9jsK4xaN4EMUu/s7a",
                            "name": "陳小菲",
                            "grade": "1",
                            "school_year": 109,
                            "department": 311
                        }, etc...
                    ],
                    "id": 311,
                    "name": "醫資系"
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Student informations for a department with departmentID 3111.

        (department not exist.)
        ```json
        {
            "success": false,
            "message": "No Found DepartmentId: 3111."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Create Student

    Create a new Student.

    - **URL** : `/student`

    - **Method** : `POST`

    - **Parameters** :

        ```json
        [
            {
                "name": "name",
                "required": true,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "grade",
                "required": false,
                "default": "1",
                "enum": [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ],
                "type": "string",
                "in": "body"   
            },
            {
                "name": "departmemt",
                "required": true,
                "type": "number",
                "in": "body",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Create a new Student, and data:

        ```json
        {
            "name": "Test_Student",
            "grade": "4",
            "department": 311
        }
        ```

        ```json
        {
            "success": true,
            "message": "Add Student Success.",
            "response": {
                "id": 109311003,
                "password": "$2a$10$PN2lkq85kqa0aOR7IdigR.4zo9Rdj74vXE.y9SEkHednnw7l8cgEe",
                "name": "Test_Student",
                "grade": "4",
                "school_year": 109,
                "department": 311
            }
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** 

        Create a new Student, and data:

        (miss name.)
        ```json
            {
                "grade": 100,
                "department": 311
            }

            {
                "success": false,
                "message": "Invalid new record.\nDetails:\n  Missing value for required attribute `name`.  Expected a string, but instead, got: undefined\n"
            }
        ```

        (grade out of range.)
        ```json
            {
                "name": "Test_Student",
                "grade": 100,
                "department": 311
            }

            {
                "success": false,
                "message": "Invalid new record.\nDetails:\n  Could not use specified `grade`.  Violated one or more validation rules:\n  • Value ('100') was not in the configured whitelist (1, 2, 3, 4, 5, 6, 7, 8, 9)\n\n"
            }
        ```

        (department not exist.)
        ```json
            {
                "name": "Test_Student",
                "grade": 4,
                "department": 3111
            }

            {
                "success": false,
                "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
            }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Update Student

    Update a Student's information.

    - **URL** : `/student/:studentId`

    - **Method** : `PUT`

    - **Parameters** :

        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path"
            },
            {
                "name": "name",
                "required": false,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "password",
                "required": false,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "grade",
                "required": false,
                "enum": [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ],
                "type": "string",
                "in": "body"   
            },
            {
                "name": "departmemt",
                "required": false,
                "type": "number",
                "in": "body",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Update a Student's information with studentID 109311003, and data:

        ```json
        {
            "name": "Test_Student111",
            "password": "Test_Student_PassWord",
            "grade": "1",
            "department": 312
        }
        ```

        ```json
        {
            "success": true,
            "message": "Update Student Success.",
            "response": {
                "id": 109311003,
                "password": "$2a$10$PVh1v5KNUrerAhbBBDOm5.Mf3l/6Ih0m1.CFnOB2Ft1TB97U8WRkW",
                "name": "Test_Student111",
                "grade": "1",
                "school_year": 109,
                "department": 312
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**

        Update a Student's information with studentID 123321123.

        (student not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example**

        Update a Student's information with studentID 109311003, and data:

        (grade out of range.)
        ```json
        {
            "grade": "10"
        }

        {
            "success": false,
            "message": "Cannot perform update with the provided values.\nDetails:\n  Could not use specified `grade`.  Violated one or more validation rules:\n  • Value ('10') was not in the configured whitelist (1, 2, 3, 4, 5, 6, 7, 8, 9)\n\n"
        }
        ```

        (department not exist.)
        ```json
        {
            "department": 123
        }

        {
            "success": false,
            "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Delete Student

    Delete a Student.

    - **URL** : `/student/:studentId`

    - **Method** : `DELETE`

    - **Parameters** : `None`: 

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**
        
        Delete a Student with studentID 109311003.

        ```json
        {
            "success": true,
            "message": "Delete Student Success."
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
             
        Delete one Student with studentID 109311003 twice.

        (student not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 109311003."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Teachers

    Display information for all Teachers.

    - **URL** : `/teachers`

    - **Method** : `GET`

    - **Parameters** :`None`

    - **Success Response:**
      - **Code** : 200 
      - **Content Example**

        Display information for all Teachers.

        ```json
        {
            "success": true,
            "message": "Found All Teachers Success.",
            "response": [
                {
                    "id": 1311001,
                    "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                    "name": "張家文",
                    "department": 311
                },
                {
                    "id": 1312001,
                    "password": "$2a$10$KSgxgYbp0gQWhPWFBhdaCOKLmZDUSjzDbU52ixSybY4sItMj0suVC",
                    "name": "吳沛鑫",
                    "department": 312
                }, etc...
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Teacher

    Display information for a Teacher.

    - **URL** : `/teacher/:teacherId`

    - **Method** : `GET`

    - **Parameters** : 

        ```json
        [
            {
                "name": "teacherId",
                "required": true,
                "type": "number",
                "in": "path"   
            }
        ]
        ```

    - **Success Response:**
      - **Code** : 200 
      - **Content Example**
        
        Display information for a Teacher with teacherID 1311001.

        ```json
        {
            "success": true,
            "message": "Found Teacher Success.",
            "response": {
                "id": 1311001,
                "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                "name": "張家文",
                "department": 311
            }
        }
        ```

    - **Error Response:**
      - **Code** : 400
      - **Content Example**
        
        Display information for a Teacher with teacherID 999.

        (teacher not exist.)
        ```json
        {
            "success": false,
            "message": "No Found TeacherId: 999."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Course's Teacher

    Display the Teacher information for a course.

    - **URL** : `/teacher/course/:courseId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display the Teacher information for a course with courseID 2311001.

        ```json
        {
            "success": true,
            "message": "Found Course's Teacher Success.",
            "response": [
                {
                    "id": 2311001,
                    "name": "程式規劃",
                    "grade": "1",
                    "teacher": {
                        "id": 1311001,
                        "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                        "name": "張家文",
                        "department": 311
                    },
                    "department": 311
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display the Teacher information for a course with courseID 123321123.

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Department's Teachers

    Display all Teacher informations for a department.

    - **URL** : `/teachers/department/:departmentId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "departmentId",
                "required": true,
                "type": "number",
                "in": "path"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Teacher informations for a deaprtment with departmentID 311.

        ```json
        {
            "success": true,
            "message": "Found All Department's Students Success.",
            "response": [
                {
                    "students": [
                        {
                            "id": 109311001,
                            "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                            "name": "王小明",
                            "grade": "1",
                            "school_year": 109,
                            "department": 311
                        },
                        {
                            "id": 109311002,
                            "password": "$2a$10$K5Z34avd2oyoGlSPL3jyO.TW0DcikhJQsbMt9jsK4xaN4EMUu/s7a",
                            "name": "陳小菲",
                            "grade": "1",
                            "school_year": 109,
                            "department": 311
                        }
                    ],
                    "id": 311,
                    "name": "醫資系"
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Teacher informations for a deaprtment with departmentID 3111.

        (department not exist.)
        ```json
        {
            "success": false,
            "message": "No Found DepartmentId: 3111."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Create Teacher

    Create a new Teacher.

    - **URL** : `/teacher`

    - **Method** : `POST`

    - **Parameters** :

        ```json
        [
            {
                "name": "name",
                "required": true,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "departmemt",
                "required": true,
                "type": "number",
                "in": "body",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Create a new Teacher, and data:

        ```json
        {
            "name": "Test_Teacher",
            "department": 311
        }
        ```

        ```json
        {
            "success": true,
            "message": "Add Teacher Success.",
            "response": {
                "id": 1311002,
                "password": "$2a$10$5aSBVh1pLqTb3VhirQTGKe96Kd/ecLSegl7JB3SjH/Qt/7GG9QUPi",
                "name": "Test_Teacher",
                "department": 311
            }
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** 

        Create a new Student, and data:

        (miss name.)
        ```json
            {
                "department": 311
            }

            {
                "success": false,
                "message": "Invalid new record.\nDetails:\n  Missing value for required attribute `name`.  Expected a string, but instead, got: undefined\n"
            }
        ```

        (department not exist.)
        ```json
            {
                "name": "Test_Teacher",
                "department": 3111
            }

            {
                "success": false,
                "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
            }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Update Teacher

    Update a Teacher's information.

    - **URL** : `/teacher/:teacherId`

    - **Method** : `PUT`

    - **Parameters** :

        ```json
        [
            {
                "name": "name",
                "required": false,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "password",
                "required": false,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "departmemt",
                "required": false,
                "type": "number",
                "in": "body",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Update a Teacher's information with teacherID 1311002, and data:

        ```json
        {
            "name": "Test_Teacher123",
            "password": "Test_Teacher_PassWord",
            "department": 312
        }
        ```

        ```json
        {
            "success": true,
            "message": "Update Teacher Success.",
            "response": {
                "id": 1311002,
                "password": "$2a$10$c0aYkk3L.jih8UYAPF3Ac.7SX0nyOgIRTp8JC7RBsbs5aMo30vnK.",
                "name": "Test_Teacher123",
                "department": 312
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
      
        Update a Teacher's information with teacherID 123321123.

        (teacher not exist.)
        ```json
        {
            "success": false,
            "message": "No Found TeacherId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example**

        Update a Teacher's information with teacherID 1311002, and data:

        (department not exist.)
        ```json
        {
            "department": 123
        }

        {
            "success": false,
            "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

## Delete Teacher

    Delete a Teacher.

- **URL** : `/teacher/:teacherId`

- **Method** : `DELETE`

- **Parameters** `None`: 

- **Success Response:**

  - **Code** : 200 
  - **Content Example**
    
    Delete a Teacher with teacherID 1311002.

    ```json
    {
        "success": true,
        "message": "Delete Teacher Success."
    }
    ```

- **Error Response:**
  - **Code** : 400
  - **Content Example**
    
    Delete a Teacher with teacherID 1311002 twice.

    (teacher not exist.)
    ```json
    {
        "success": false,
        "message": "No Found TeacherId: 1311002."
    }
    ```

  - **Code** : 500
  - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Courses

    Display information for all Courses.

    - **URL** : `/courses`

    - **Method** : `GET`

    - **Parameters** :`None`

    - **Success Response:**
      - **Code** : 200 
      - **Content Example**

        Display information for all Courses.

        ```json
        {
            "success": true,
            "message": "Found All Courses Success.",
            "response": [
                {
                    "id": 2311001,
                    "name": "程式規劃",
                    "grade": "1",
                    "teacher": {
                        "id": 1311001,
                        "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                        "name": "張家文",
                        "department": 311
                    },
                    "department": 311
                },
                {
                    "id": 2311002,
                    "name": "資料庫",
                    "grade": "1",
                    "teacher": {
                        "id": 1311001,
                        "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                        "name": "張家文",
                        "department": 311
                    },
                    "department": 311
                }, etc...
            ]
        }
        ```

    - **Error Response:**
      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Course

    Display information for a Course.

    - **URL** : `/course/:courseId`

    - **Method** : `GET`

    - **Parameters** : 

        ```json
        [
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path"   
            }
        ]
        ```

    - **Success Response:**
      - **Code** : 200 
      - **Content Example**
        
        Display information for a Course with courseID 2313001.

        ```json
        {
            "success": true,
            "message": "Found Course Success.",
            "response": {
                "id": 2313001,
                "name": "基礎護理",
                "grade": "1",
                "teacher": {
                    "id": 1313001,
                    "password": "$2a$10$/SvdfP7Xrmvuk16HpDhDoeUVCsePKAoXg9ke481j3wLu6dIYKcn4y",
                    "name": "張德華",
                    "department": 313
                },
                "department": 313
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display information for a Course with courseID 123321123.

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 123321123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Student's Courses

    Display all Course informations for a student.

    - **URL** : `/courses/student/:studentId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Student"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Course informations for a student with studentID 109311001.

        ```json
        {
            "success": true,
            "message": "Found All Student's Courses Success.",
            "response": [
                {
                    "courses": [
                        {
                            "id": 2311001,
                            "name": "程式規劃",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        },
                        {
                            "id": 2311002,
                            "name": "資料庫",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        }
                    ],
                    "id": 109311001,
                    "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                    "name": "王小明",
                    "grade": "1",
                    "school_year": 109,
                    "department": 311
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Course informations for a student with studentID 123321.

        (student not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 123321."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Teacher's Courses

    Display all Course informations for a teacher.

    - **URL** : `/courses/teacher/:teacherId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "teacherId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Teacher"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Course informations for a teacher with teacherID 1311001.

        ```json
        {
            "success": true,
            "message": "Found All Teacher's Courses Success.",
            "response": [
                {
                    "courses": [
                        {
                            "id": 2311001,
                            "name": "程式規劃",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        },
                        {
                            "id": 2311002,
                            "name": "資料庫",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        }
                    ],
                    "id": 1311001,
                    "password": "$2a$10$NpRq2ZPCi9ysRxsZn5pieObLZUc.Imo3RBYS1k4Gv6SG550TE4uKm",
                    "name": "張家文",
                    "department": 311
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Course informations for a teacher with teacherID 123321.

        (teacher not exist.)
        ```json
        {
            "success": false,
            "message": "No Found TeacherId: 123321."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Department's Courses

    Display all Course informations for a department.

    - **URL** : `/courses/department/:departmentId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "departmentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Course informations for a department with deaprtmentID 311.

        ```json
        {
            "success": true,
            "message": "Found All Department's Courses Success.",
            "response": [
                {
                    "courses": [
                        {
                            "id": 2311001,
                            "name": "程式規劃",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        },
                        {
                            "id": 2311002,
                            "name": "資料庫",
                            "grade": "1",
                            "teacher": 1311001,
                            "department": 311
                        }, etc...
                    ],
                    "id": 311,
                    "name": "醫資系"
                }
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Course informations for a department with deaprtmentID 3111.

        (department not exist.)
        ```json
        {
            "success": false,
            "message": "No Found DepartmentId: 3111."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Create Course

    Create a new Course.

    - **URL** : `/course`

    - **Method** : `POST`

    - **Parameters** :

        ```json
        [
            {
                "name": "name",
                "required": true,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "grade",
                "required": false,
                "default": "1",
                "enum": [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ],
                "type": "string",
                "in": "body"   
            },
            {
                "name": "teacher",
                "required": true,
                "type": "number",
                "in": "body",
                "reference": "Teacher"
            },
            {
                "name": "departmemt",
                "required": true,
                "type": "number",
                "in": "body",
                "reference": "Department"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Create a new Course, and data:

        ```json
        {
            "name": "Test_Course",
            "grade": "4",
            "teacher": 1312001,
            "department": 313
        }
        ```

        ```json
        {
            "success": true,
            "message": "Add Course Success.",
            "response": {
                "id": 2313003,
                "name": "Test_Course",
                "grade": "4",
                "teacher": 1312001,
                "department": 313
            }
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** 

        Create a new Course, and data:

        (miss name.)
        ```json
            {
                "grade": "4",
                "teacher": 1312001,
                "department": 313
            }

            {
                "success": false,
                "message": "Invalid new record.\nDetails:\n  Missing value for required attribute `name`.  Expected a string, but instead, got: undefined\n"
            }
        ```

        (grade out of range.)
        ```json
            {
                "name": "Test_Course",
                "grade": 100,
                "teacher": 1312311,
                "department": 313
            }

            {
                "success": false,
                "message": "Invalid new record.\nDetails:\n  Could not use specified `grade`.  Violated one or more validation rules:\n  • Value ('100') was not in the configured whitelist (1, 2, 3, 4, 5, 6, 7, 8, 9)\n\n"
            }
        ```

        (teacher not exist.)
        ```json
            {
                "name": "Test_Course",
                "grade": "4",
                "teacher": 111111111,
                "department": 313
            }

            {
                "success": false,
                "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
            }
        ```

        (department not exist.)
        ```json
            {
                "name": "Test_Course",
                "grade": "4",
                "teacher": 1312001,
                "department": 123
            }

            {
                "success": false,
                "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
            }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Update Course

    Update a Course's information.

    - **URL** : `/course/:courseId`

    - **Method** : `PUT`

    - **Parameters** :

        ```json
        [
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path"
            },
            {
                "name": "name",
                "required": false,
                "type": "string",
                "in": "body"   
            },
            {
                "name": "grade",
                "required": false,
                "enum": [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ],
                "type": "string",
                "in": "body"   
            },
            {
                "name": "teacher",
                "required": false,
                "type": "number",
                "in": "body",
                "reference": "Teacher"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Update a Course's information with courseID 2313003, and data:

        ```json
        {
            "name": "Test_Course",
            "grade": "5",
            "teacher": 1312001
        }
        ```

        ```json
        {
            "success": true,
            "message": "Update Course Success.",
            "response": {
                "id": 2313003,
                "name": "Test_Course",
                "grade": "5",
                "teacher": 1312001,
                "department": 313
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
      
        Update a Course's information with courseID 123321

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 123321."
        }
        ```

      - **Code** : 500
      - **Content Example**
        
        Update a Course's information with courseID 123321, and data:

        (grade out of range.)
        ```json
        {
            "grade": "100"
        }

        {
            "success": false,
            "message": "Cannot perform update with the provided values.\nDetails:\n  Could not use specified `grade`.  Violated one or more validation rules:\n  • Value ('100') was not in the configured whitelist (1, 2, 3, 4, 5, 6, 7, 8, 9)\n\n"
        }
        ```

        (teacher not exist.)
        ```json
        {
            "teacher": 111
        }

        {
            "success": false,
            "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Delete Course

    Delete a Course.

    - **URL** : `/course/:courseId`

    - **Method** : `DELETE`

    - **Parameters** `None`: 

    - **Success Response:**

      - **Code** : 200 
      - **Content Example**
        
        Delete a Course with courseID 2313003.

        ```json
        {
            "success": true,
            "message": "Delete Course Success."
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Delete a Course with courseID 2313003 twice.

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 1311002."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Select Course

    Student select a Course.

    - **URL** : `/student/:studentId/course/:coursesId`

    - **Method** : `POST`

    - **Parameters** : 
      
        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Student"
            },
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"
            }
        ]
        ```

    - **Success Response:**

      - **Code** : 200 
      - **Content Example**
        
        Student 109311002 select Course 2311001

        ```json
        {
            "success": true,
            "message": "Select One Course Success."
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example**
        
        Student 109311002 select Course 2311001 twice.
        (repeat select)
        ```json
        {
            "success": false,
            "message": "Would violate uniqueness constraint-- a record already exists with conflicting value(s)."
        }
        ```

        Student 123 select Course 2311001.
        (student not exist.)
        ```json
        {
            "success": false,
            "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
        }
        ```

        Student 109311002 select Course 123.
        (course not exist.)
        ```json
        {
            "success": false,
            "message": "Unexpected error from database adapter: ER_NO_REFERENCED_ROW_2: Cannot add or update a child row: a foreign key constraint fails."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## UnSelect Course

    Student UnSelect a Course.

    - **URL** : `/student/:studentId/course/:courseId`

    - **Method** : `DELETE`

    - **Parameters** : 
      
        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Student"
            },
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"
            }
        ]
        ```

    - **Success Response:**

      - **Code** : 200 
      - **Content Example**
        
        Student 109311002 Unselect Course 2311001

        ```json
        {
            "success": true,
            "message": "UnSelect One Course Success."
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Register Score

    Teacher register student score.

    - **URL** : `/score/student/:studentId/course/:courseId/_score/:score`

    - **Method** : `PUT`

    - **Parameters** : 
      
        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Student"
            },
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"
            },
            {
                "name": "_score",
                "required": true,
                "type": "number",
                "in": "path"
            }
        ]
        ```

    - **Success Response:**

      - **Code** : 200 
      - **Content Example**
        
        Register Student: 109311002, Course: 2311001, score: 100

        ```json
        {
            "success": true,
            "message": "Register Score Success.",
            "response": {
                "id": 16,
                "score": 100,
                "student": 109311002,
                "course": 2311001
            }
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Student's Scores

    Display all Scores of a student.

    - **URL** : `/scores/student/:studentId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path"   
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Scores of a student with studentID 109311001.

        ```json
        {
            "success": true,
            "message": "Found One Student's All Scores Success.",
            "response": {
                "id": 109311001,
                "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                "name": "王小明",
                "grade": "1",
                "school_year": 109,
                "department": 311,
                "scores": [
                    {
                        "id": 1,
                        "score": 100,
                        "courseId": 2311001,
                        "courseName": "程式規劃"
                    },
                    {
                        "id": 2,
                        "score": 0,
                        "courseId": 2311002,
                        "courseName": "資料庫"
                    }
                ]
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Scores of a student with studentID 123.

        (student not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show Course's Scores

    Display all Scores of a course.

    - **URL** : `/scores/course/:courseId`

    - **Method** : `GET`

    - **Parameters** :

        ```json
        [
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"
            }
        ]
        ```

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display all Scores of a course with courseID 2311001.

        ```json
        {
            "success": true,
            "message": "Found One Course's All Scores Success.",
            "response": {
                "id": 2311001,
                "name": "程式規劃",
                "grade": "1",
                "teacher": 1311001,
                "department": 311,
                "scores": [
                {
                    "id": 1,
                    "score": 100,
                    "studentId": 109311001,
                    "studentName": "王小明"
                },
                {
                    "id": 16,
                    "score": 100,
                    "studentId": 109311002,
                    "studentName": "陳小菲"
                }
                ]
            }
        }
        ```

    - **Error Response:**

      - **Code** : 400
      - **Content Example**
        
        Display all Scores of a course with courseID 123.

        (course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found CourseId: 123."
        }
        ```

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show All Scores

    Display Scores for all students and all courses.

    - **URL** : `/scores`

    - **Method** : `GET`

    - **Parameters** : `None`

    - **Success Response:**
      
      - **Code** : 200 
      - **Content Example**

        Display Scores for all students and all courses.

        ```json
        {
            "success": true,
            "message": "Found All Score Success.",
            "response": [
                {
                    "id": 1,
                    "score": 100,
                    "student": {
                        "id": 109311001,
                        "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                        "name": "王小明",
                        "grade": "1",
                        "school_year": 109,
                        "department": 311
                    },
                    "course": {
                        "id": 2311001,
                        "name": "程式規劃",
                        "grade": "1",
                        "teacher": 1311001,
                        "department": 311
                    }
                },
                {
                    "id": 2,
                    "score": 0,
                    "student": {
                        "id": 109311001,
                        "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                        "name": "王小明",
                        "grade": "1",
                        "school_year": 109,
                        "department": 311
                    },
                    "course": {
                        "id": 2311002,
                        "name": "資料庫",
                        "grade": "1",
                        "teacher": 1311001,
                        "department": 311
                    }
                }, etc...
            ]
        }
        ```

    - **Error Response:**

      - **Code** : 500
      - **Content Example** : [**Server Error**](#server-error)

---

  - ## Show One Score

    Display a student's Score in a course.

    - **URL** : `/score/student/:studentId/course/:courseId`
    
    - **Method** : `GET`
    
    - **Parameters** : 
    
        ```json
        [
            {
                "name": "studentId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Student"
            },
            {
                "name": "courseId",
                "required": true,
                "type": "number",
                "in": "path",
                "reference": "Course"
            }
        ]
        ```

    - **Success Response:**
    
    - **Code** : 200 
    - **Content Example**
        
        Display student 109311001's Score in course 2311001.

        ```json
        {
            "success": true,
            "message": "Found One Student's One Course Score Success.",
            "response": {
                "id": 1,
                "score": 100,
                "student": {
                    "id": 109311001,
                    "password": "$2a$10$4DtDLbGKJOY6qY77sgSvlu3GSiGJBy2os3kXwh7xO2LFUvRIc0MZK",
                    "name": "王小明",
                    "grade": "1",
                    "school_year": 109,
                    "department": 311
                },
                "course": {
                    "id": 2311001,
                    "name": "程式規劃",
                    "grade": "1",
                    "teacher": 1311001,
                    "department": 311
                }
            }
        }
        ```

    - **Error Response:**
    
    - **Code** : 400
    - **Content Example**

        Display student 123's Score in course 123.
        (student or course not exist.)
        ```json
        {
            "success": false,
            "message": "No Found StudentId: 109311001 or CourseId: 123."
        }
        ```

    - **Code** : 500
    - **Content Example** : [**Server Error**](#server-error)

---

# Schema

 - ## Student

    - **id**
      - `Type: INT`
      - `AllowNull: false`
      - `Primary Key`
      - `Unique Key`
    - **password**
      - `Type: VARCHAR (100)`
      - `AllowNull: false`
    - **name**
      - `Type: VARCHAR (50)`
      - `AllowNull: false`
    - **grade**
      - `Type: ENUM ['1','2','3','4','5','6','7','8','9']`
      - `AllowNull: false`
      - `Default: '1'`
    - **school_year**
      - `Type: INT`
      - `AllowNull: false`
    - **department**
      - `Type: INT`
      - `AllowNull: false`
      - `Foreign Key reference 'department(id)' ON UPDATE CASCADE ON DELETE CASCADE`

 - ## Teacher

    - **id**
      - `Type: INT`
      - `AllowNull: false`
      - `Primary Key`
      - `Unique Key`
    - **password**
      - `Type: VARCHAR (100)`
      - `AllowNull: false`
    - **name**
      - `Type: VARCHAR (50)`
      - `AllowNull: false`
    - **department**
      - `Type: INT`
      - `AllowNull: false`
      - `Foreign Key reference 'department(id)' ON UPDATE CASCADE ON DELETE CASCADE`

 - ## Course

    - **id**
      - `Type: INT`
      - `AllowNull: false`
      - `Primary Key`
      - `Unique Key`
    - **name**
      - `Type: VARCHAR (50)`
      - `AllowNull: false`
    - **grade**
      - `Type: ENUM ['1','2','3','4','5','6','7','8','9']`
      - `AllowNull: false`
      - `Default: '1'`
    - **teacher**
      - `Type: INT`
      - `AllowNull: false`
      - `Foreign Key reference 'teacher(id)' ON UPDATE CASCADE ON DELETE CASCADE`
    - **department**
      - `Type: INT`
      - `AllowNull: false`
      - `Foreign Key reference 'department(id)' ON UPDATE CASCADE ON DELETE CASCADE`

 - ## Elective

    - **id**
      - `Type: INT`
      - `AllowNull: false`
      - `Auto Increment`
      - `Primary Key`
    - **score**
      - `Type: INT`
      - `AllowNull: false`
      - `Default: 0`
    - **student**
      - `Type: INT`
      - `AllowNull: false`
      - `Unique Key`
      - `Foreign Key reference 'student(id)' ON UPDATE CASCADE ON DELETE CASCADE`
    - **course**
      - `Type: INT`
      - `AllowNull: false`
      - `Unique Key`
      - `Foreign Key reference 'course(id)' ON UPDATE CASCADE ON DELETE CASCADE`

 - ## Department

    - **id**
      - `Type: INT`
      - `AllowNull: false`
      - `Primary Key`
      - `Unique Key`
    - **name**
      - `Type: VARCHAR (50)`
      - `AllowNull: false`

# Note

- ## Server Error

  Other Error Response.

  ```json
  {
      "success": false,
      "message": *Error_message
  }
  ```