const bcryptjs = require('bcryptjs');

module.exports = {
    attributes: {
        id: {
            type: 'number',
            required: true,
        },
        password: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (100)'
        },
        name: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (50)',
        },
        grade: {
            type: 'string',
            isIn: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            defaultsTo: '1',
        },
        school_year: {
            type: 'number',
            required: true,
        },
        department: {
            model: 'department',
            required: true,
        },

        // electives:{
        //     collection: 'elective',
        //     via:'student'
        // },
        courses: {
            collection: 'course',
            via: 'student',
            through: 'elective'
        }
    },
    beforeCreate: async function (value, next) {

        //計算密碼
        //預設密碼為hash過的id
        const oldPassword = value.password;
        const salt = bcryptjs.genSaltSync(10);
        const newPassword = bcryptjs.hashSync(oldPassword, salt);
        value.password = newPassword;

        next();
    },
    beforeUpdate: async function(value, next){

        if(value.password){
            
            const oldPassword = value.password;
            const salt = bcryptjs.genSaltSync(10);
            const newPassword = bcryptjs.hashSync(oldPassword, salt);

            value.password = newPassword;
        }

        next();
    }
}