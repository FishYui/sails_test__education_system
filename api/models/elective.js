module.exports = {
    attributes: {
        id: {
            type: 'number',
            autoIncrement: true,
        },
        score: {
            type: 'number',
            defaultsTo: 0,
        },
        student: {
            model: 'student',
            required: true
        },
        course: {
            model: 'course',
            required: true
        }
    }
}