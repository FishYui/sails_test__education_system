module.exports = {
    attributes: {
        id: {
            type: 'number',
            required: true,
        },
        name: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (50)',
        },
        grade: {
            type: 'string',
            isIn: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
            defaultsTo: '1',
        },
        teacher: {
            model: 'teacher',
            required: true,
        },
        department: {
            model: 'department',
            required: true,
        },

        // electives:{
        //     collection: 'elective',
        //     via:'course'
        // },
        students: {
            collection: 'student',
            via: 'course',
            through: 'elective'
        }
    }
}