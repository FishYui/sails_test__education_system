module.exports = {
    attributes: {
        id: {
            type: 'number',
            required: true,
        },
        name: {
            type: 'string',
            required: true,
            columnType: 'VARCHAR (50)',
        },

        students: {
            collection: 'student',
            via: 'department'
        },
        courses: {
            collection: 'course',
            via: 'department'
        },
        teachers: {
            collection: 'teacher',
            via: 'department'
        }
    }
}