module.exports = {
    //查看所有課程
    show_courses: async (req, res) => {

        try {

            const response = await course.find({}).populate('teacher');

            res.status(200).json({ success: true, message: 'Found All Courses Success.', response: response })

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某一門課程
    show_course: async (req, res) => {

        const courseId = req.params.courseId;

        try {

            const response = await course.findOne({ id: courseId }).populate('teacher');

            if (!response) {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            } else {
                res.status(200).json({ success: true, message: `Found Course Success.`, response: response });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某位學生選修的所有課程
    show_student_courses: async (req, res) => {

        const studentId = req.params.studentId;

        try {

            const response = await student.find({ id: studentId }).populate('courses');

            if (response.length != 0) {
                res.status(200).json({ success: true, message: 'Found All Student\'s Courses Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某位教師教的所有課程
    show_teacher_courses: async (req, res) => {

        const teacherId = req.params.teacherId;

        try {

            const response = await teacher.find({ id: teacherId }).populate('courses');

            if (response.length != 0) {
                res.status(200).json({ success: true, message: 'Found All Teacher\'s Courses Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found TeacherId: ${teacherId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某個系所有的課程資訊
    show_department_courses: async (req, res) => {

        const departmentId = req.params.departmentId;

        try {

            const response = await department.find({ id: departmentId }).populate('courses');

            if (response.length != 0) {
                res.status(200).json({ success: true, message: 'Found All Department\'s Courses Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found DepartmentId: ${departmentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },

    //新增一門課程
    add_course: async (req, res) => {

        const { name, grade, teacher, department } = req.body;

        try {
            //計算所屬科系已有多少門課
            let courseCount = await course.count({ department: department });
            //算出課程id => 2(開頭為1: 教師、開頭為2: 課程) + 系所編號 + 系所當前擁有的課程數量+1(前面補0)
            courseCount = (++courseCount).toString();
            const id = `2${department + '0'.repeat(Math.abs(3 - courseCount.length)) + courseCount}`;

            const response = await course.create({ id, name, grade, teacher, department }).fetch();

            res.status(200).json({ success: true, message: 'Add Course Success.', response: response });
            
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //修改一門課程
    update_course: async (req, res) => {

        const courseId = req.params.courseId;
        const { name, grade, teacher } = req.body;

        try {

            const response = await course.updateOne({ id: courseId }).set({ name, grade, teacher });

            if (response) {
                res.status(200).json({ success: true, message: 'Update Course Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //刪除一門課程
    delete_course: async (req, res) => {

        const courseId = req.params.courseId;

        try {

            const response = await course.destroyOne({ id: courseId });

            if (response) {
                res.status(200).json({ success: true, message: 'Delete Course Success.' });
            } else {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            }
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}