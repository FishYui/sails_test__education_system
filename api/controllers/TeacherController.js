module.exports = {
    //查看所有教師
    show_teachers: async (req, res) => {

        try {

            const response = await teacher.find({});

            res.status(200).json({ success: true, message: 'Found All Teachers Success.', response: response })

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某位教師
    show_teacher: async (req, res) => {

        const teacherId = req.params.teacherId;

        try {

            const response = await teacher.findOne({ id: teacherId });

            if (response) {
                res.status(200).json({ success: true, message: 'Found Teacher Success.', response: response })
            } else {
                res.status(400).json({ success: false, message: `No Found TeacherId: ${teacherId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某門課的授課教師
    show_course_teacher: async (req, res) => {

        const courseId = req.params.courseId;

        try {

            const response = await course.find({ id: courseId }).populate('teacher');

            if (response.length != 0) {
                res.status(200).json({ success: true, message: 'Found Course\'s Teacher Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某個系所有的教師
    show_department_teachers: async (req, res) => {

        const departmentId = req.params.departmentId;

        try {

            const response = await department.find({ id: departmentId }).populate('teachers');

            if (response.length != 0) {
                res.status(200).json({ success: true, message: 'Found All Department\'s Teachers Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found DepartmentId: ${departmentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //新增一位教師
    add_teacher: async (req, res) => {

        const { name, department } = req.body;

        try {
            //計算該系已有多少教師
            let teacherCount = await teacher.count({ department: department });
            //當前教師數量+1 = 教師流水號 (轉成字串方便補0)
            let number = (++teacherCount).toString();
            //教師流水號未滿3位數補0
            number = '0'.repeat(Math.abs(3 - number.length)) + number;
            //教師代號(1) + 系所編號 + 教師流水號 = 教師編號
            const id = `1${department}${number}`;
            const password = id.toString();

            const response = await teacher.create({ id, password, name, department }).fetch();

            res.status(200).json({ success: true, message: 'Add Teacher Success.', response: response });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //修改一位教師
    update_teacher: async (req, res) => {

        const teacherId = req.params.teacherId;
        const { password, name, department } = req.body;

        try {

            const response = await teacher.updateOne({ id: teacherId }).set({ password, name, department });

            if (response) {
                res.status(200).json({ success: true, message: 'Update Teacher Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found TeacherId: ${teacherId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //刪除一位教師
    delete_teacher: async (req, res) => {

        const teacherId = req.params.teacherId;

        try{

            const response = await teacher.destroyOne({ id: teacherId });

            if (response) {
                res.status(200).json({ success: true, message: 'Delete Teacher Success.' });
            } else {
                res.status(400).json({ success: false, message: `No Found TeacherId: ${teacherId}.` });
            }

        }catch(err){
            res.status(500).json({ success: false, message: err.message });
        }
    }
}