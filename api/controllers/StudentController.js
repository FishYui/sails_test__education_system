module.exports = {
    //查看所有學生
    show_students: async (req, res) => {

        try {

            const response = await student.find({});

            res.status(200).json({ success: true, message: 'Found All Students Success.', response: response })

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某位學生
    show_student: async (req, res) => {

        const studentId = req.params.studentId;

        try {

            const response = await student.findOne({ id: studentId });

            if (response) {
                res.status(200).json({ success: true, message: 'Found Student Success.', response: response })
            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某門課所有的選修學生
    show_course_students: async (req, res) => {

        const courseId = req.params.courseId;

        try {

            const response = await course.findOne({ id: courseId }).populate('students');

            if (response) {
                res.status(200).json({ success: true, message: 'Found All Course\'s Students Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某個系所有的學生
    show_department_students: async (req, res) => {

        const departmentId = req.params.departmentId;

        try {

            const response = await department.findOne({ id: departmentId }).populate('students');

            if (response) {
                res.status(200).json({ success: true, message: 'Found All Department\'s Students Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found DepartmentId: ${departmentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //新增一位學生
    add_student: async (req, res) => {

        const { name, grade, department } = req.body;
        //模擬學年，以新增當下的月份+日期，要大於3位數，不足補0
        const date = new Date();
        const school_year = date.getFullYear() - 1911;

        try {
            //計算該系該學年已有多少學生
            let studentCount = await student.count({ department: department, school_year: school_year });
            //當前學生數量+1 = 此學生流水號 (轉成字串方便補0)
            let number = (++studentCount).toString();
            //此學生流水號未滿3位數補0
            number = '0'.repeat(Math.abs(3 - number.length)) + number;
            //學年 + 系所編號 + 此學生流水號 = 學號
            const id = `${school_year}${department}${number}`;
            const password = id.toString();

            const response = await student.create({ id, password, name, grade, school_year, department }).fetch();

            res.status(200).json({ success: true, message: 'Add Student Success.', response: response });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //修改一位學生
    update_student: async (req, res) => {

        const studentId = req.params.studentId;
        const { name, password, grade, department } = req.body;

        try {

            const response = await student.updateOne({ id: studentId }).set({ name, password, grade, department });

            if (response) {
                res.status(200).json({ success: true, message: 'Update Student Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //刪除一位學生
    delete_student: async (req, res) => {

        const studentId = req.params.studentId;

        try {

            const response = await student.destroyOne({ id: studentId });

            if (response) {
                res.status(200).json({ success: true, message: 'Delete Student Success.' });
            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId}.` });
            }
            
        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}