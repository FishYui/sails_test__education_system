module.exports = {
    //學生選修一門課
    select_course: async (req, res) => {

        const studentId = req.params.studentId;
        const coursesId = req.params.coursesId;

        try {
            await student.addToCollection(studentId, 'courses', coursesId);

            res.status(200).json({ success: true, message: 'Select One Course Success.' });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //學生退選一門課
    unselect_course: async (req, res) => {

        const studentId = req.params.studentId;
        const coursesId = req.params.courseId;

        try {
            await student.removeFromCollection(studentId, 'courses').members(coursesId);

            res.status(200).json({ success: true, message: 'UnSelect One Course Success.' });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },

    //查看某位學生的所有成績
    show_student_scores: async (req, res) => {

        const studentId = req.params.studentId;

        try {
            //取得學生資料
            const response = await student.findOne({ id: studentId });

            //判斷是否有這位學生
            if (response) {

                //取得該學生的所有成績和對應的課程資料
                const scores = await elective.find({
                    where: { student: studentId },
                    select: ['id', 'score', 'course']
                }).populate('course');

                //只取課程資料中的課程名稱和ID
                scores.map((score) => {
                    score["courseId"] = score.course.id;
                    score["courseName"] = score.course.name;
                    delete score.course;
                });

                //彙整
                response['scores'] = scores;

                res.status(200).json({ success: true, message: 'Found One Student\'s All Scores Success.', response: response });

            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看某門課程的所有成績
    show_course_scores: async (req, res) => {

        const courseId = req.params.courseId;

        try {
            //取得課程資料
            const response = await course.findOne({ id: courseId });

            //判斷是否有這門課程
            if (response) {

                //取得該課程的所有成績和對應的學生資料
                const scores = await elective.find({
                    where: { course: courseId },
                    select: ['id', 'score', 'student']
                }).populate('student');

                //只取學生資料中的學生名稱
                scores.map((score) => {
                    score["studentId"] = score.student.id;
                    score["studentName"] = score.student.name;
                    delete score.student;
                });

                //彙整
                response["scores"] = scores;

                res.status(200).json({ success: true, message: 'Found One Course\'s All Scores Success.', response: response });

            } else {
                res.status(400).json({ success: false, message: `No Found CourseId: ${courseId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    },
    //查看所有課程、所有學生的成績
    show_scores: async (req, res) => {

        try {

            const response = await elective.find({}).populate('student').populate('course');

            res.status(200).json({ success: true, message: 'Found All Score Success.', response: response });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }

    },
    //查看某位學生的某門課程成績
    show_score: async (req, res) => {

        const studentId = req.params.studentId;
        const courseId = req.params.courseId;

        try {

            const response = await elective.findOne({ student: studentId, course: courseId }).populate('student').populate('course');

            if (response) {
                res.status(200).json({ success: true, message: 'Found One Student\'s One Course Score Success.', response: response });
            } else {
                res.status(400).json({ success: false, message: `No Found StudentId: ${studentId} or CourseId: ${courseId}.` });
            }

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }


    },

    //教師登記成績
    register_score: async (req, res) => {

        const studentId = req.params.studentId;
        const courseId = req.params.courseId;
        const score = req.params.score;

        try {

            const response = await elective.updateOne({ student: studentId, course: courseId }).set({ score: score });

            res.status(200).json({ success: true, message: 'Register Score Success.', response: response });

        } catch (err) {
            res.status(500).json({ success: false, message: err.message });
        }
    }
}